from gurobipy import *

file_path = 'input/Class_01.2bp'

instance_number = -1
instances_per_file = 50
instances_list = []
with open(file_path) as f:
    for k in range(0,instances_per_file):
        instance_number = instance_number + 1
        instance_object = {}

        # read the instance class
        x = f.readline().rstrip()
        numbers = [int(s) for s in x.split() if s.isdigit()]
        instance_object['class'] = numbers[0]

        # read number of items
        x = f.readline().rstrip()
        numbers = [int(s) for s in x.split() if s.isdigit()]
        instance_object['n_itens'] = numbers[0]

        # get instance number
        x = f.readline().rstrip()
        numbers = [int(s) for s in x.split() if s.isdigit()]
        instance_object['relative_num'] = numbers[0]
        instance_object['absolute_num'] = numbers[1]
        instance_object['read_num'] = instance_number

        # get instance bin size
        x = f.readline().rstrip()
        numbers = [int(s) for s in x.split() if s.isdigit()]
        instance_object['bin_h'] = numbers[0]
        instance_object['bin_w'] = numbers[1]

        # read the items size
        itens_info = []
        for i in range(0,instance_object['n_itens']):
            x = f.readline().rstrip()
            numbers = [int(s) for s in x.split() if s.isdigit()]
            itens_info.append((numbers[0], numbers[1]))

        instance_object['itens'] = itens_info

        # skip empty line
        x = f.readline().rstrip()
        instances_list.append(instance_object)


# ============================================
# =========GUROBI MODEL ======================
#
m = model("2dbp")


