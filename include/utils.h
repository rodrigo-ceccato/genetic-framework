#ifndef UTILS_H
#define UTILS_H
#include <stdlib.h>
#include <random>
#include <omp.h>

#define TRUE  1
#define FALSE 0

//#define VERBOSE

int get_random(int min, int max);
void set_seed(int seed);

#endif

