#ifndef DEFAULT_GA_H
#define DEFAULT_GA_H
#include "utils.h"
#include "BotLeftHeuristic.h"
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>
// #define VERBOSE

#define WHEEL_SELECTION         658
#define PAIR_SELECTION          906
#define TOURNAMENT_SELECTION    824
#define CROSSOVER_NON_LINEAR    302
#define CROSSOVER_ORDERED       202
#define SEQUENCE_MUTATE         358
#define SWAP_MUTATE             458
#define PASS_GENERATION         558
#define ELITIST_GENERATION      404
#define STEADY_STATE_GENERATION 970


class DefaultGA
{
    private:

        long int bestValue; // keeps best balue so far
        int generations; // number of generations before stopping
        int popSize; // number of individuals
        int chromosomeSize; // size of a chromosome
        int mutationRate; // change of a mutation ocurring, per locus
        int **population;
        int *bestChromossome;
        BotLeftHeuristic PlaceHeuristic;
        // void (*mutationFunction)(int *chromosome, int rate);

    public:
        DefaultGA(int popSize, int generations, int mutationRate, int chromosomeSize, BotLeftHeuristic instance);
        ~DefaultGA();
        void crossover2points (int *parentA, int *parentB, int *childA, int *childB);
        void crossoverOrdered (int *parentA, int *parentB, int *childA, int *childB);
        void crossoverNonLinear(int *parentA, int *parentB, int *childA, int *childB);
        void swapMutate(int *chromosome, int r);
        void sequenceMutate(int *chromosome, int rate);
        void generatePermutation(int *chromosome);
        void solve(int max_time_seconds, int selection_method, int crossover_method,
                     int mutation_method, int generation_method);
        void solveAM(int max_time_seconds, int selection_method, int crossover_method,
                     int mutation_method, int generation_method); 
        void showPopulation();
        void passGeneration(int **offspring, int dummy);
        void elitistGeneration(int **offspring, int dummy);
        void steadyStateGeneration (int **offspring, int replaced);
        int dummyFitness(int *chromosome);
        void pairSelection(int **population, int *parentA, int *parentB, int i);
        void wheelSelection(int **population, int *parentA, int *parentB, int dummy);
        void tournamentSelection(int **population, int *parentA, int *parentB, int tournament_dimension);
        static bool mysort (int *chromosome1, int *chromosome2);
        void showChromosome(int *chromosome);
        int fitness (int *chromosome);
        int *getBestChromossome();
        void generateInitialPopulation();
};

#endif

