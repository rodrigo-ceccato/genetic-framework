/*
 * This file defines some util
 * funtions to standarize
 * their utilization
 * along the differente aproachs
 * to GA algoritms
 */

#include "utils.h"

void set_seed(int seed){
    srand(seed);
}

int get_random(int min, int max){
    return rand() % (max - min + 1) + min ;
}


