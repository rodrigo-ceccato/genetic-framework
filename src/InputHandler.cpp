#include "InputHandler.h"

InputHandler::InputHandler(char input_file_path[], int numOfInstances){
    #ifdef VERBOSE
    printf("Reading input: %s\n", input_file_path);
    #endif

    InputHandler::numOfInstances = numOfInstances;

    InputHandler::inputInstances = (inputInstance *)malloc(sizeof(inputInstance) * numOfInstances);

    std::ifstream input( input_file_path );
    std::string line;

    int i = 0;

    int temp_w;
    int temp_h;
    while(i < numOfInstances){
        /*
         * Expected file format:
         * (int) PROBLEM CLASS
         * (int) num of itens
         * (int) (int) relative | absolute n. of instance
         * (int) (int) bin_h | bin_w
         * (int) (int) item_i_h | item_i_w
         * */

        input >> inputInstances[i].problemClass;
        //TODO: there are better ways to do this...
        getline(input,line); // discart rest of line
        input >> inputInstances[i].sizesArray_size;
        inputInstances[i].sizesArray_size++; // we will put
                                             //the bin size there too
        getline(input,line);
        input >> inputInstances[i].relativeNumber >> inputInstances[i].absoluteNumber;
        getline(input,line);
        inputInstances[i].sizesArray = (int**)malloc(sizeof(int*) * inputInstances[i].sizesArray_size);
        for(int j = 0; j < inputInstances[i].sizesArray_size; j++){
            inputInstances[i].sizesArray[j] = (int*)malloc(sizeof(int) * 2);
            input >> temp_h >> temp_w;
            inputInstances[i].sizesArray[j][H] = (int)temp_h;
            inputInstances[i].sizesArray[j][W] = (int)temp_w;
            getline(input,line);
        }

        #ifdef VERBOSE
        printf("Added inputInstance[%d]:\n", i);
        printf("problemClass = %d\n", inputInstances[i].problemClass);
        printf("sizesArray_size = %d\n", inputInstances[i].sizesArray_size);
        printf("relativeNumber = %d\n", inputInstances[i].relativeNumber);
        printf("absoluteNumber = %d\n", inputInstances[i].absoluteNumber);
        for(int j = 0; j < inputInstances[i].sizesArray_size; j++){
            printf(">>>>[%d, %d]\n", inputInstances[i].sizesArray[j][H], inputInstances[i].sizesArray[j][W]);
        }
        #endif

        i++;
    }
}

InputHandler::~InputHandler(){
    for(int i = 0; i < getNumOfInstances(); i++){
        free(inputInstances[i].sizesArray);
    }
    free(inputInstances);
}

int InputHandler::getInstanceClass(int i){
    return inputInstances[i].problemClass;
}

int **InputHandler::getSizesArray(int i){
    return inputInstances[i].sizesArray;
}

int InputHandler::getSize(int i){
    return inputInstances[i].sizesArray_size;
}

int InputHandler::getNumOfInstances(){
    return InputHandler::numOfInstances;
}
