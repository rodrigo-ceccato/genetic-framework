#include "DefaultGA.h"

DefaultGA::DefaultGA(int popSize, int generations, int mutationRate, int chromosomeSize, BotLeftHeuristic instance) : PlaceHeuristic(instance)
{
    DefaultGA::generations = generations;
    DefaultGA::popSize = popSize;
    DefaultGA::mutationRate = mutationRate;
    DefaultGA::chromosomeSize = chromosomeSize;

    /* allocate memory to store best chromosome found so far */
    bestChromossome = (int*)malloc(sizeof(int) * chromosomeSize);

    /* allocate array of individuals (chromosomes) */
    DefaultGA::population = (int**)malloc(sizeof(int*) * popSize);
    for(int i = 0; i < popSize; i++){
        DefaultGA::population[i] = (int*)malloc(sizeof(int) * chromosomeSize);
    }
}

DefaultGA::~DefaultGA(void){
    /* free memory allocated in constructor */
    for(int i = 0; i < popSize; i++){
        free(DefaultGA::population[i]);
    }

    free(DefaultGA::population);
    free(bestChromossome);
}

/* performs two-point cross-over over two vectors
 * saving results to two childs pointers*/
void DefaultGA::crossover2points(int *parentA, int *parentB,
        int *childA, int *childB) {
    int size = DefaultGA::chromosomeSize;
    // two points cross over
    // x1 - x2 - x3 -x4 -x5 -x6
    // y1 - y2 - y3 -y4 -y5 -y6
    //      *        *
    //      ^        ^
    //      |        |
    //   cross-over points
    // x1-x2 -y3-y4-x5-x6

    /*
     * get the two random crossing points
     */
    int crossingPointA = get_random(1,(size-3));
    int crossingPointB = get_random(crossingPointA+1, size-2);

    /*
     * get the sequences from first parante
     * that will go on A:
     * childA: x1 - x2 ... p1 y3 - y4 ... p2 x4 - x5
     */
    for(int i = 0; i < crossingPointA; i++){
        childA[i] = parentA[i];
        childB[i] = parentB[i];
    }

    /*
     * get the sequence that comes
     * from the second parent
     */
    for(int i = crossingPointA; i < crossingPointB; i++){
        childA[i] = parentB[i];
        childB[i] = parentA[i];
    }

    /*
     * get the last part from the first parent
     */
    for (int i = crossingPointB; i < DefaultGA::chromosomeSize; i++){
        childA[i] = parentA[i];
        childB[i] = parentB[i];
    }
}

/* Ordered crossover
 * this method produces a valid permutation
 * by selecting a random subset from the first
 * parent and then filling the remaining space
 * with the values from the other parent,
 * in the order that they appear there
 * */
void DefaultGA::crossoverOrdered(int *parentA, int *parentB,
        int *childA, int *childB){

    int size = DefaultGA::chromosomeSize;

    /*
     * A: x1 x2 x3 x4 x5
     *            ^
     * B: x3 x5 x2 x4 x1
     *
     * c: x1 x2 x3 | x5 x4
     *============================
     * x1 x2 x3 where copied from A and
     * x4 and x5 where the remaning allels
     * so they where added in the order that
     * they appear in B
     */
    int slice_pos = get_random(1, size-2);
    int missing_elementA[size];
    int missing_elementB[size];

    /* inicializes vetor of missing elements */
    for(int i=0; i < size; i++) {
        missing_elementA[i] = 1;
        missing_elementB[i] = 1;
    }

    /* mark the chromosomes that are already
     * in the child */
    for(int i = 0; i < slice_pos; i++){
        /* indicates this element is already in
         * the child */
        missing_elementA[(int)parentA[i]] = 0;
        childA[i] = parentA[i];

        missing_elementB[(int)parentB[i]] = 0;
        childB[i] = parentB[i];
    }

    /* completes the missing space in each children*/
    int childAWritePos = slice_pos;
    int childBWritePos = slice_pos;
    for(int i = 0; i < size; i++){
        if (missing_elementA[(int)parentB[i]] == 1)
            childA[childAWritePos++] = parentB[i];

        if (missing_elementB[(int)parentA[i]] == 1)
            childB[childBWritePos++] = parentA[i];
    }

}

/*
 * Perform non-linear cross-over.
 * Example:
 * Parents:
 * A: 5 2 1 3 4 0
 * B: 3 4 1 0 5 2
 *     x     x
 *     (crossing points = 1,4)
 *
 *  Exchanges:
 *     2 <=> 4
 *     1 <=> 1
 *     3 <=> 0
 *
 *  Childs:
 *  5 4 1 0 2 3
 *  0 2 1 3 5 4
 */
void DefaultGA::crossoverNonLinear(int *parentA, int *parentB, int *childA, int *childB) {
    int size = DefaultGA::chromosomeSize;

    int slice_pos1 = get_random(1, size-2);
    int slice_pos2 = get_random(slice_pos1, size-2);

    int exchanges[slice_pos2 - slice_pos1][2];

    int j = 0;
    for (int i = slice_pos1; i < slice_pos2; i++) {
        exchanges[j][0] = parentA[i];
        exchanges[j][1] = parentB[i];
        j++;
    }

    for (int i = 0; i < size; i++) {
        childA[i] = parentA[i];
        childB[i] = parentB[i];
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < slice_pos2 - slice_pos1; j++) {
            if (childA[i] == exchanges[j][0]) {
                childA[i] = exchanges[j][1];
            }
            else if (childA[i] == exchanges[j][1]) {
                childA[i] = exchanges[j][0];
            }
            if (childB[i] == exchanges[j][0]) {
                childB[i] = exchanges[j][1];
            }
            else if (childB[i] == exchanges[j][1]) {
                childB[i] = exchanges[j][0];
            }
        }
    }
}

/*
 * Perform swap mutation at a
 * given chromosome.
 * Given a rate r,
 * and a chromosome along its size
 * input: rate = int from 0 to 100
 */
void DefaultGA::swapMutate(int *chromosome, int rate){
    int size = DefaultGA::chromosomeSize;

    /* rools the dice for each locus */
    for(int i = 0; i < size-1; i++) {
        /* if the roll is sucessful,
         * i.e. dice value <= rate,
         * perfom a random swap*/
        int dice = get_random(1,100);
        if(dice <=  rate){
            /*
             * Performs a swap with the gene
             * at a random locus of index
             * greater than i.
             * Example:
             *
             * [ x1 x2 x3 ... xn]
             *   ^(i)
             *         ^(replaceIndex)
             * swaps with some random index greater than i
             */

            int temp = chromosome[i];
            int replaceIndex = get_random(i+1, size-1);
            chromosome[i]            = chromosome[replaceIndex];
            chromosome[replaceIndex] = temp;
        }
    }
}

/*
 * Performs sequence mutation.
 * Example:
 *   2 4 1 3 5 0
 *   x  x
 *   (slice random points)
 *
 *   Result:
 *   1 3 5 0 2 4
 * */
void DefaultGA::sequenceMutate(int *chromosome, int rate) {
    int size = DefaultGA::chromosomeSize;

    int dice = get_random(1,100);
    if (dice > rate) {
        int slice_pos1 = get_random(0, size-1);
        int slice_pos2 = get_random(slice_pos1, size-1);

        int mutate_chromosome[size];

        int j = 0;
        for (int i = 0; i < slice_pos1; i++) {
            mutate_chromosome[j] = chromosome[i];
            j++;
        }

        for (int i = slice_pos2; i < size; i++) {
            mutate_chromosome[j] = chromosome[i];
            j++;
        }

        for (int i = slice_pos1; i < slice_pos2; i++) {
            mutate_chromosome[j] = chromosome[i];
            j++;
        }

        for (int i = 0; i < size; i++) {
            chromosome[i] = mutate_chromosome[i];
        }
    }

}

/*
 * Performs a generation passage.
 * In this method, every individual of the offspring
 * replaces the previous generation.
 */
void DefaultGA::passGeneration (int **offspring, int dummy) {
    for (int i = 0; i < DefaultGA::popSize; i++) {
        for (int j = 0; j < DefaultGA::chromosomeSize; j++) {
            DefaultGA::population[i][j] = offspring[i][j];
        }
    }
}

// steady state only adds some chromosome to the new population
/*
 * Performs steady state generation passage.
 * This method replaces only a number of individuals
 * of the population with the offspring.
 * The individuals replaced are the ones with the
 * lowest fitness.
 * The number of individuals to be replaced is a parameter:
 *      n_replaced.
 */
void DefaultGA::steadyStateGeneration (int **offspring, int n_replaced) {
    int fitnessOfAll[DefaultGA::popSize][2];
    for (int i = 0; i < DefaultGA::popSize; i++) {
        int fit = dummyFitness(population[i]);

        fitnessOfAll[i][1] = fit;
        fitnessOfAll[i][0] = i;
    }

    /*
     * Use selection sort to sort the population array
     */
    for (int i = 0; i < DefaultGA::popSize; i++) {
        int j = i;
        for (int k = i+1; k < DefaultGA::popSize; k++){
            if (fitnessOfAll[k][1] < fitnessOfAll[j][1]) {
                j = k;
            }
        }
        int ind = fitnessOfAll[i][0];
        int fitness = fitnessOfAll[i][1];
        fitnessOfAll[i][0] = fitnessOfAll[j][0];
        fitnessOfAll[i][1] = fitnessOfAll[j][1];
        fitnessOfAll[j][0] = ind;
        fitnessOfAll[j][1] = fitness;
    }

    for (int i = 0; i < n_replaced; i++) {
        int j = fitnessOfAll[i][0];
        memcpy(population[j], offspring[i], DefaultGA::chromosomeSize * sizeof(int));
    }


}

/*
 * Generate the initial population
 * with random individuals
 */
void DefaultGA::generateInitialPopulation() {
    for(int i = 0; i < popSize; i++)
        DefaultGA::generatePermutation(DefaultGA::population[i]);
}

/*
 * Performs elitist generation passage.
 * This method replaces the all but one individual
 * from the previus generation with the offspring.
 */
void DefaultGA::elitistGeneration (int **offspring, int dummy) {
    int bestFit = 0;
    int fit;
    int fittest[DefaultGA::chromosomeSize];

    /* find the fittest chromosome
     * from the previous population */
    for (int i = 0; i < DefaultGA::popSize; i++) {
        fit = dummyFitness(DefaultGA::population[i]);
        if (fit > bestFit) {
            bestFit = fit;
            memcpy(fittest, DefaultGA::population[i], DefaultGA::chromosomeSize*sizeof(int));

        }
    }

    /*
     * Keep the fittest individual from the
     * previus generation in the new one.
     * */
    memcpy(DefaultGA::population[0], fittest, DefaultGA::chromosomeSize*sizeof(int));

    /*
     * add the other (popSize - 1)
     * chromosomes to the new population
     */
    for (int i = 1; i < DefaultGA::popSize; i++) {
        for (int j = 0; j < DefaultGA::chromosomeSize; j++) {
            DefaultGA::population[i][j] = offspring[i][j];
        }
    }
}

/*
 * Generates a random permutation
 * with values from 0 to chromosomeSize
 * by generating the sorted chromosome
 * 0,1,...,n
 * and then mutating it
 */
void DefaultGA::generatePermutation(int *chromosome){
    int size = DefaultGA::chromosomeSize;

    for(int i=0;i<size;i++)
        chromosome[i]=i;

    DefaultGA::swapMutate(chromosome, 100);
    DefaultGA::swapMutate(chromosome, 100);
}

/*
 * Copy a the individuals i and i+1
 * to two poiters.
 * This is an auxiliary function that returns
 * two individuals that are adjacent
 */
void DefaultGA::pairSelection(int **population, int *parentA, int *parentB, int i) {
    int size = DefaultGA::chromosomeSize;
    memcpy(parentA, population[i], size * sizeof(int));
    memcpy(parentB, population[i + 1], size * sizeof(int));
}

/*
 * Performs Wheel Selection
 * This parent selection method
 * get two individuals, selected to be parents,
 * biased by their fitness value
 */
void DefaultGA::wheelSelection(int **population, int *parentA, int *parentB, int dummy)  {
    int totalFitness = 0;
    int fitnessOfAll[DefaultGA::popSize][2];

    /*
     * In parallel, calculates the fitness of
     * every individual in the population
     */
    #pragma omp parallel for
    for (int i = 0; i < DefaultGA::popSize; i++) {
        int fit = dummyFitness(population[i]);
        fitnessOfAll[i][1] = fit;
        fitnessOfAll[i][0] = i;
    }

    /*
     * computate total fitness
     */
    for (int i = 0; i < DefaultGA::popSize; i++) {
        totalFitness += fitnessOfAll[i][1];
    }

    int rng = get_random(0, totalFitness - 1);

    /*
     * Use selection sort to sort the population
     * by the fitness values of the individuals
     */
    for (int i = 0; i < DefaultGA::popSize; i++) {
        int j = i;
        for (int k = i+1; k < DefaultGA::popSize; k++){
            if (fitnessOfAll[k][1] > fitnessOfAll[j][1]) {
                j = k;
            }
        }
        int ind = fitnessOfAll[i][0];
        int fitness = fitnessOfAll[i][1];
        fitnessOfAll[i][0] = fitnessOfAll[j][0];
        fitnessOfAll[i][1] = fitnessOfAll[j][1];
        fitnessOfAll[j][0] = ind;
        fitnessOfAll[j][1] = fitness;
    }

    int partial_sum = 0;
    int rng2 = 0;

    for (int i = 0; i < DefaultGA::popSize; i++) {
        partial_sum += fitnessOfAll[i][1];
        if (partial_sum > rng) {
            memcpy(parentA, population[fitnessOfAll[i][0]], DefaultGA::chromosomeSize * sizeof(int));
            rng2 = get_random(0, totalFitness - fitnessOfAll[i][1] - 1);
            fitnessOfAll[i][1] = 0;
            break;
        }
    }

    partial_sum = 0;
    for (int i = 0; i < DefaultGA::popSize; i++) {

        partial_sum += fitnessOfAll[i][1];
        if (partial_sum > rng2) {
            memcpy(parentB, population[fitnessOfAll[i][0]], DefaultGA::chromosomeSize * sizeof(int));
            break;
        }
    }
}

/*
 * Performs tournament selection
 * This method choses k random individuals from
 * the population. The fittest wins the tournament.
 * This function performs 2 tourneys to select
 * 2 parents.
 */
void DefaultGA::tournamentSelection(int **population, int *parentA, int *parentB, int tournament_dimension)  {
    int participants[tournament_dimension][DefaultGA::chromosomeSize];
    int size = DefaultGA::popSize;

    /*
     * Performs the first tournament
     */
    for (int i = 0; i < tournament_dimension; i++) {
        int rng = get_random(0, size - 1);
        memcpy(participants[i], population[rng], DefaultGA::chromosomeSize * sizeof(int));
    }
    int j = 0;
    int bestFit = 0;
    for (int i = 0; i < tournament_dimension; i++) {
        int fit = dummyFitness(participants[i]);
        if (fit > bestFit) {
            j = i;
            bestFit = fit;
        }
    }
    memcpy(parentA, participants[j], DefaultGA::chromosomeSize * sizeof(int));

    /*
     * Performs the second tournament
     */
    for (int i = 0; i < tournament_dimension; i++) {
        int rng = get_random(0, size - 1);
        memcpy(participants[i], population[rng], DefaultGA::chromosomeSize * sizeof(int));
    }

    j = 0;
    bestFit = 0;
    for (int i = 0; i < tournament_dimension; i++) {
        int fit = dummyFitness(participants[i]);
        if (fit > bestFit) {
            j = i;
            bestFit = fit;
        }
    }
    memcpy(parentB, participants[j], DefaultGA::chromosomeSize * sizeof(int));
    // showChromosome(parentA); //DEBUG
    // showChromosome(parentB); //DEBUG
}

/*
 * Debug function:
 * prints all individuals from
 * the current population
 */
void DefaultGA::showPopulation(){
    for(int i = 0; i < DefaultGA::popSize; i++){
        printf("Individual %d = [ ", i);
        for(int temp = 0; temp < chromosomeSize; temp++)
            printf("%d ",DefaultGA::population[i][temp]);
        printf("]\n");
    }
}
void DefaultGA::showChromosome(int *chromosome) {
    printf("Individual = [ ");
        for(int temp = 0; temp < chromosomeSize; temp++)
            printf("%d ",chromosome[temp]);
        printf("]\n");
}

/*
 * Placeholder function used before
 * the fitness function was implemented.
 * TODO: replace all dummyFitness usage with
 * proper fitness function
 */
int DefaultGA::dummyFitness (int *chromosome) {
    return DefaultGA::fitness(chromosome);
}

/*
 * Returns the fitness value of a
 * given chromosome.
 * The value is based on the total number
 * of bins needed by the chromosome.
 * Fewer bins means higher fitness value.
 */
int DefaultGA::fitness (int *chromosome) {
    int numOfBins = DefaultGA::PlaceHeuristic.numOfBins(chromosome);
    int numOfItens = DefaultGA::PlaceHeuristic.getNumOfItens();
    //printf("solution value == %d\n", numOfBins);
    //printf("fitness == %d\n", numOfItens - numOfBins);
    return numOfItens-numOfBins;
}

/*
 * Getter:
 * returns the best chromsome found
 * after running the solve() method
 */
int* DefaultGA::getBestChromossome(){
    return DefaultGA::bestChromossome;
}

/*
 * Solves an instance of GA,
 * returning the best individual
 */
void DefaultGA::solve(int max_time_seconds, int selection_method, int crossover_method,
                     int mutation_method, int generation_method){
    int currentFitness = -1;

    void (DefaultGA::*selectionFunction)(int**, int*, int*, int);
    void (DefaultGA::*crossOverFunction)(int*, int*, int*, int*);
    void (DefaultGA::*mutationFunction)(int*, int);
    void (DefaultGA::*generationFunction)(int**, int);

    if (selection_method == WHEEL_SELECTION) {
        selectionFunction = &DefaultGA::wheelSelection;
    }
    else if(selection_method == TOURNAMENT_SELECTION) {
        selectionFunction = &DefaultGA::tournamentSelection;
    }
    else {
        printf("unknow selection method, the caller is crazy\n\n");
        exit(1);
    }
    if (crossover_method == CROSSOVER_NON_LINEAR) {
        crossOverFunction = &DefaultGA::crossoverNonLinear;
    }
    else if(crossover_method == CROSSOVER_ORDERED) {
        crossOverFunction = &DefaultGA::crossoverOrdered;
    }
    else {
        printf("unknow crossover method, the caller is crazy\n\n");
        exit(1);
    }
    if (mutation_method == SEQUENCE_MUTATE) {
        mutationFunction = &DefaultGA::sequenceMutate;
    }
    else if (mutation_method == SWAP_MUTATE) {
        mutationFunction = &DefaultGA::swapMutate;
    } else {
        printf("unknow mutation method, the caller is crazy\n\n");
        exit(1);
    }
    if (generation_method == ELITIST_GENERATION) {
        generationFunction = &DefaultGA::elitistGeneration;
    }
    else if (generation_method == STEADY_STATE_GENERATION) {
        generationFunction = &DefaultGA::steadyStateGeneration;
    }
    else if (generation_method == PASS_GENERATION) {
        generationFunction = &DefaultGA::passGeneration;
    }
    else {
        printf("unknow generation method, the caller is crazy\n\n");
        exit(1);
    }

    /* generates the inicial population */
    DefaultGA::generateInitialPopulation();

    /* allocates memory for offspring calculations */
    int **offspring = (int**)malloc(popSize * sizeof(int*));
    for(int i = 0; i < popSize; i++){
        offspring[i] = (int*)malloc(sizeof(int) * chromosomeSize);
    }

    time_t start_time,current_time;
    time (&start_time);
    int elapsed = 0;
    int elapsedGens = 0;

    for(int i = 0; i < generations && elapsed <= max_time_seconds; i++){
        /* generating offspring */
        #pragma omp parallel for
        for(int i = 0; i < popSize; i = i + 2){
            int parentA[DefaultGA::chromosomeSize];
            int parentB[DefaultGA::chromosomeSize];

            (*this.*selectionFunction)(DefaultGA::population, parentA, parentB, (int)(DefaultGA::popSize*0.3));

            (*this.*crossOverFunction)(parentA, parentB, offspring[i], offspring[i+ 1]);

        }

        /* apply mutation to the offspring*/
        #pragma omp parallel for
        for(int i = 0; i < popSize; i++)
            (*this.*mutationFunction)((int*)offspring[i], DefaultGA::mutationRate);

        // New geneartion selection strategies
        (*this.*generationFunction)(offspring, (int)(DefaultGA::popSize*0.1));

        #ifdef VERBOSE
            printf("New generation = %d\n", i);
            DefaultGA::showPopulation();
            printf("====================\n");
        #endif

         //update the BEST CHROMOSSOME found so far
         int changeFlag = 0;
         for(int i =0; i < popSize; i++){
            if(DefaultGA::fitness(population[i]) > currentFitness) {
                changeFlag = 1;
                memcpy(bestChromossome, population[i], DefaultGA::chromosomeSize * sizeof(int));
                currentFitness = DefaultGA::fitness(bestChromossome);
            }
         }

        time (&current_time);
        elapsed = (int) difftime(current_time,start_time);
        elapsedGens++;

        /* report when new solution is found */
        if (changeFlag)
             printf("Updating best sol to: %d(n_bin=%d) @ gen = %d (t=%ds)\n",
                    currentFitness, PlaceHeuristic.numOfBins(bestChromossome), i,elapsed);
    }

    printf("Elapsed generations: %d\n", elapsedGens);

    /* free allocated memory */
    for(int i = 0; i < popSize; i++){
        free(offspring[i]);
    }
    free(offspring);
}


/*
 * Solves an instance of GA,
 * returning the best individual
 */
void DefaultGA::solveAM(int max_time_seconds, int selection_method, int crossover_method,
                     int mutation_method, int generation_method){
    int popSize = DefaultGA::popSize;
    int chromosomeSize = DefaultGA::chromosomeSize;
    int generations = DefaultGA::generations;
    int currentFitness = -1;
    int max_radiation = mutationRate * 4;

    void (DefaultGA::*selectionFunction)(int**, int*, int*, int);
    void (DefaultGA::*crossOverFunction)(int*, int*, int*, int*);
    void (DefaultGA::*mutationFunction)(int*, int);
    void (DefaultGA::*generationFunction)(int**, int);

    if (selection_method == WHEEL_SELECTION) {
        selectionFunction = &DefaultGA::wheelSelection;
    }
    else if(selection_method == TOURNAMENT_SELECTION) {
        selectionFunction = &DefaultGA::tournamentSelection;
    }
    else {
        printf("unknow selection method, the caller is crazy\n\n");
        exit(1);
    }
    if (crossover_method == CROSSOVER_NON_LINEAR) {
        crossOverFunction = &DefaultGA::crossoverNonLinear;
    }
    else if(crossover_method == CROSSOVER_ORDERED) {
        crossOverFunction = &DefaultGA::crossoverOrdered;
    }
    else {
        printf("unknow crossover method, the caller is crazy\n\n");
        exit(1);
    }
    if (mutation_method == SEQUENCE_MUTATE) {
        mutationFunction = &DefaultGA::sequenceMutate;
    }
    else if (mutation_method == SWAP_MUTATE) {
        mutationFunction = &DefaultGA::swapMutate;
    } else {
        printf("unknow mutation method, the caller is crazy\n\n");
        exit(1);
    }
    if (generation_method == ELITIST_GENERATION) {
        generationFunction = &DefaultGA::elitistGeneration;
    }
    else if (generation_method == STEADY_STATE_GENERATION) {
        generationFunction = &DefaultGA::steadyStateGeneration;
    }
    else if (generation_method == PASS_GENERATION) {
        generationFunction = &DefaultGA::passGeneration;
    }
    else {
        printf("unknow generation method, the caller is crazy\n\n");
        exit(1);
    }

    DefaultGA::generateInitialPopulation();
    /* generates the inicial population */
    /* and allocates memory for offspring calculations */
    int **offspring = (int**)malloc(popSize * sizeof(int*));
    for(int i = 0; i < popSize; i++){
        offspring[i] = (int*)malloc(sizeof(int) * chromosomeSize);
    }

    time_t start_time,current_time;
    time (&start_time);
    int elapsed;
    int generation_without_improv = 0;

    int old_mutation_rate = DefaultGA::mutationRate;

    for(int i = 0; i < generations && elapsed <= max_time_seconds; i++){
        /* generating offspring */
        #pragma omp parallel for
        for(int i = 0; i < popSize; i = i + 2){
            int parentA[DefaultGA::chromosomeSize];
            int parentB[DefaultGA::chromosomeSize];

            (*this.*selectionFunction)(DefaultGA::population, parentA, parentB, (int)(DefaultGA::popSize*0.3));

            (*this.*crossOverFunction)(parentA, parentB, offspring[i], offspring[i+ 1]);
        }

        /* apply mutation to the offspring*/
        #pragma omp parallel for
        for(int i = 0; i < popSize; i++)
            (*this.*mutationFunction)((int*)offspring[i], DefaultGA::mutationRate);

        (*this.*generationFunction)(offspring, (int)(DefaultGA::popSize*0.1));

        #ifdef VERBOSE
            printf("New generation = %d\n", i);
            DefaultGA::showPopulation();
            printf("====================\n");
        #endif

         //update the BEST CHROMOSSOME SO FAR!!
         int changeFlag = 0;
         for(int i =0; i < popSize; i++){
            if(DefaultGA::fitness(population[i]) > currentFitness) {
                changeFlag = 1;
                memcpy(bestChromossome, population[i], DefaultGA::chromosomeSize * sizeof(int));
                currentFitness = DefaultGA::fitness(bestChromossome);
            }
         }

        time (&current_time);
        elapsed = (int) difftime (current_time,start_time);

        /* report when new solution is found */
        if (changeFlag){
             printf("Updating best sol to: %d(n_bin=%d) @ gen = %d (t=%ds)\n",
                    currentFitness, PlaceHeuristic.numOfBins(bestChromossome), i,elapsed);
             generation_without_improv = 0;
             DefaultGA::mutationRate = old_mutation_rate;
             printf("Mutation rate = %d\n", DefaultGA::mutationRate);
        } else{
            /*  count the number of generations not improving */
            generation_without_improv += 1;
        }

        /* if too much generations do not show improvmente, update the mutate rate */
        if(((int)generation_without_improv*20) / generations == 1) {
            printf(">>ALERT!, %d gens without improvment!\n", generation_without_improv);
            if(DefaultGA::mutationRate * 1.5 <= max_radiation)
                DefaultGA::mutationRate = (int)DefaultGA::mutationRate * 1.5;
            printf(">>Rising radiation in chamber. New mutation rate = %d\n", DefaultGA::mutationRate);
            generation_without_improv = 0;
        }
    }

    /* free allocated memory */
    for(int i = 0; i < popSize; i++){
        free(offspring[i]);
    }
    free(offspring);
}
