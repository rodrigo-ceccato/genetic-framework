#include "utils.h"
#include "DefaultGA.h"
#include "BotLeftHeuristic.h"
#include "InputHandler.h"
#include "main.h"

class GAQueue
{
    private:
        std::queue<int *> instanceInfo;

    public:
        void runNext(std::ofstream& data_sheet)
        {
            int *instance_meta_info = instanceInfo.front();
            char sheet_buffer[1000] = {0};
            //get input instance
            int instanceNumber = instance_meta_info[PROB_INSTANCE];
            char instanceFile[PATH_MAX_SIZE];
            sprintf(instanceFile, "input/Class_%02d.2bp", instance_meta_info[PROB_CLASS]);
            InputHandler cp1(instanceFile, INSTANCES_PER_FILE);

            //open files for writing
            char output_path[PATH_MAX_SIZE];
            sprintf(output_path, PACKING_DATA_OUTPUT_PATH);
            strcat(output_path, "/");
            char problemInstanceName[PATH_MAX_SIZE];
            sprintf(problemInstanceName, "C%02d_i%d_%dpopsize_%dgen_%dmr_%ds", instance_meta_info[PROB_CLASS],
                    instance_meta_info[PROB_INSTANCE], instance_meta_info[PROB_POP_SIZE], instance_meta_info[PROB_N_GEN],
                    instance_meta_info[PROB_MUT_RATE], instance_meta_info[PROB_TIME_LIM]);
            strcat(output_path, problemInstanceName);
            strcat(output_path, ".txt");
            sprintf(sheet_buffer, "%02d_i%d,%d,%d,%d,", instance_meta_info[PROB_CLASS],
                    instance_meta_info[PROB_INSTANCE], instance_meta_info[PROB_POP_SIZE], instance_meta_info[PROB_N_GEN],
                    instance_meta_info[PROB_MUT_RATE]);

            // no need to close the file
            std::ofstream packing_info_file(output_path);

            if (!packing_info_file)
            {
                printf("Error prepraing file!\n");
                exit(1);
            }

            time_t start_time, end_time;
            time(&start_time);
            BotLeftHeuristic decodeInstance(cp1.getSizesArray(instanceNumber), cp1.getSize(instanceNumber));
            int *bestChromossome;
            DefaultGA GA(instance_meta_info[PROB_POP_SIZE], instance_meta_info[PROB_N_GEN],
                    instance_meta_info[PROB_MUT_RATE], decodeInstance.getNumOfItens(), decodeInstance);

            printf("=====================\n");
            printf("Starting solver for %s\n", problemInstanceName);
            printf("Selection Method = ");
            if (instance_meta_info[SOLVER_SELECTION_METHOD] == WHEEL_SELECTION)
            {
                printf("Wheel Selection");
                strcat(sheet_buffer, "Wheel Selection,");
            }
            else if (instance_meta_info[SOLVER_SELECTION_METHOD] == TOURNAMENT_SELECTION)
            {
                printf("Tournament Selection");
                strcat(sheet_buffer, "Tournament Selection,"); 
            }
            else
            {
                printf("unknow selection method, the caller is crazy\n\n");
                exit(1);
            }


            printf("\nCrossover Method = ");
            if (instance_meta_info[SOLVER_CROSSOVER_METHOD] == CROSSOVER_NON_LINEAR)
            {
                printf("Non Linear Crossover");
                strcat(sheet_buffer, "NonLinear Crossover,");
            }
            else if (instance_meta_info[SOLVER_CROSSOVER_METHOD] == CROSSOVER_ORDERED)
            {
                printf("Ordered Crossover");
                strcat(sheet_buffer, "Ordered Crossover,");
            }
            else
            {
                printf("unknow crossover method, the caller is crazy\n\n");
                exit(1);
            }


            printf("\nMutation Method = ");
            if (instance_meta_info[SOLVER_MUTATION_METHOD] == SEQUENCE_MUTATE)
            {
                printf("Sequence Mutation");
                strcat(sheet_buffer, "Sequence Mutation,");
            }
            else if (instance_meta_info[SOLVER_MUTATION_METHOD] == SWAP_MUTATE)
            {
                printf("Swap Mutation");
                strcat(sheet_buffer, "Swap Mutation,");
            }
            else
            {
                printf("unknow mutation method, the caller is crazy\n\n");
                exit(1);
            }


            printf("\nGeneration Method = ");
            if (instance_meta_info[SOLVER_GENERATION_METHOD] == ELITIST_GENERATION)
            {
                printf("Elitist Generation\n");
                strcat(sheet_buffer, "Elitist Generation,");
            }
            else if (instance_meta_info[SOLVER_GENERATION_METHOD] == STEADY_STATE_GENERATION)
            {
                printf("Steady State Generation\n");
                strcat(sheet_buffer, "Steady State Generation,");
            }
            else if (instance_meta_info[SOLVER_GENERATION_METHOD] == PASS_GENERATION)
            {
                printf("Pass Generation\n");
                strcat(sheet_buffer, "Pass Generation,");
            }
            else
            {
                printf("unknow generation method, the caller is crazy\n\n");
                exit(1);
            }
            GA.solve(instance_meta_info[PROB_TIME_LIM], instance_meta_info[SOLVER_SELECTION_METHOD],
                    instance_meta_info[SOLVER_CROSSOVER_METHOD], instance_meta_info[SOLVER_MUTATION_METHOD],
                    instance_meta_info[SOLVER_GENERATION_METHOD]);
            bestChromossome = GA.getBestChromossome();
            time(&end_time);
            int buffer_stimated_size = (40 * decodeInstance.getNumOfItens() * 2);
            char buffer[buffer_stimated_size];
            decodeInstance.printPoistionOfItems(bestChromossome, buffer);
            packing_info_file << buffer;

            double dif = difftime(end_time, start_time);
            printf("Number of bins used: %d\n", decodeInstance.numOfBins(bestChromossome));
            printf("Elasped time is %.1lf seconds.\n", dif);
            double bufferUsed = 100 * ((double)strlen(buffer)) / ((double)buffer_stimated_size);
            printf("Buffer used: [%d]/[%d] (%.2f%%)\n", (int)strlen(buffer), buffer_stimated_size, bufferUsed);
            printf("=================\n");


            sprintf(sheet_buffer + strlen(sheet_buffer), "%.1lf,%d,\n", dif, decodeInstance.numOfBins(bestChromossome));
            data_sheet << sheet_buffer;

            free(instanceInfo.front());
            instanceInfo.pop();
        }

        int isEmpty()
        {
            return instanceInfo.empty();
        }

        void push(int problemClass, int problemInstance, int populationSize,
                int numOfGenerations, int mutationRate, int timeLimitInSeconds,
                int selection_method, int crossover_method, int mutation_method, int generation_method)
        {
            int *meta_info = (int *)malloc(N_INSTANCE_META_INFO * sizeof(int));
            meta_info[PROB_CLASS] = problemClass;
            meta_info[PROB_INSTANCE] = problemInstance;
            meta_info[PROB_POP_SIZE] = populationSize;
            meta_info[PROB_N_GEN] = numOfGenerations;
            meta_info[PROB_MUT_RATE] = mutationRate;
            meta_info[PROB_TIME_LIM] = timeLimitInSeconds;
            meta_info[SOLVER_SELECTION_METHOD] = selection_method;
            meta_info[SOLVER_CROSSOVER_METHOD] = crossover_method;
            meta_info[SOLVER_MUTATION_METHOD] = mutation_method;
            meta_info[SOLVER_GENERATION_METHOD] = generation_method;

            instanceInfo.push(meta_info);
        }
};

int main(int argc, char **argv)
{
    /* if an argument is given, use it */
    if(argc != 1) {
        printf("Taking argument, setting number of cores to %d\n", atoi(argv[1]));
        /* sets the maximum number of threads */
        omp_set_num_threads(atoi(argv[1]));
    }

    /* seeds the pseudo-random number generator */
    set_seed(time(NULL));

    /* reports num of threads being used */
    printf("Running with %d cores avaliable!\n", omp_get_max_threads());

    /* Queue everything we wanna run */
    GAQueue instQueue;

    //int popSizes[3] = {100, 500, 2000};
    //int mutationRates[3] = {1, 10, 50};
    //int generationLimits[3] = {5, 100, 5000};

    for (int instClass = 1; instClass <= 1; instClass++)
    {
        for (int instNum = 0; instNum <= 49; instNum++)
        {

            /*
             * Params:
             * >Problem class
             * >Problem instance (absolute in class)
             * >Population size
             * >Num of generations
             * >Mutation rate
             * >Time limit in seconds
             */
            //instQueue.push(instClass, instNum, 2000, 1000, 5,
            //        1800, WHEEL_SELECTION, CROSSOVER_NON_LINEAR, SEQUENCE_MUTATE,
            //        STEADY_STATE_GENERATION);

            //instQueue.push(instClass, instNum, 2000, 1000, 5,
            //        1800, TOURNAMENT_SELECTION, CROSSOVER_ORDERED, SWAP_MUTATE,
            //        ELITIST_GENERATION);

            //instQueue.push(instClass, instNum, 2000, 1000, 5,
            //        1800, TOURNAMENT_SELECTION, CROSSOVER_NON_LINEAR, SEQUENCE_MUTATE,
            //        PASS_GENERATION);

            instQueue.push(instClass, instNum, 1000, 2000, 5,
                    120, WHEEL_SELECTION, CROSSOVER_ORDERED, SWAP_MUTATE,
                    ELITIST_GENERATION);
        }
    }
    char buffer[1000];
    std::ofstream data_sheet("output/data.csv");
    sprintf(buffer, "Instâncias, Population Size, Number Of Generations, Mutation Rate, Selection Method, Crossover Method, Mutation Method, New Generation Method, Elapsed Time, Best Solution,\n");
    data_sheet << buffer;
    /* run everything */
    while (!instQueue.isEmpty())
    {
        instQueue.runNext(data_sheet);
    }

    return 0;
}
